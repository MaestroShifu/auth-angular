import { Injectable } from '@angular/core';

import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from "@angular/router";

import { AuthServiceService } from "./auth-service.service";
import { audit } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardServiceService implements CanActivate {

  constructor(private auth: AuthServiceService) { 

  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean {

    console.log("Esto es next: ", next, "Esto es state: ", state);

    return this.auth.isAuthenticated();
  }
}
